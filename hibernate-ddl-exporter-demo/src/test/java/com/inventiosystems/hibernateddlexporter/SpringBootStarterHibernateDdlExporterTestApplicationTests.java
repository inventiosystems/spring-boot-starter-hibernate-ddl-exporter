/*-
 * #%L
 * Spring Boot Starter Hibernate DDL Exporter Demo
 * %%
 * Copyright (C) 2017 Inventio Systems
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.inventiosystems.hibernateddlexporter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith ( SpringExtension.class )
@SpringBootTest
@ActiveProfiles ( value = { "db-h2" } )
public class SpringBootStarterHibernateDdlExporterTestApplicationTests {

    @Test
    public void contextLoads () {
        // verify context loads
    }

}
