/*-
 * #%L
 * Spring Boot Starter Hibernate DDL Exporter Demo
 * %%
 * Copyright (C) 2017 Inventio Systems
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.inventiosystems.hibernateddlexporter.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
public class Customer extends AbstractPersistable<Long> {

    @Column ( nullable = false )
    private String firstname;

    private String surname;

    private String street;

    private String city;

    private Integer zipcode;

    private String country;

    public String getFirstname () {
        return firstname;
    }

    public void setFirstname ( String firstname ) {
        this.firstname = firstname;
    }

    public String getSurname () {
        return surname;
    }

    public void setSurname ( String surname ) {
        this.surname = surname;
    }

    public String getStreet () {
        return street;
    }

    public void setStreet ( String street ) {
        this.street = street;
    }

    public String getCity () {
        return city;
    }

    public void setCity ( String city ) {
        this.city = city;
    }

    public Integer getZipcode () {
        return zipcode;
    }

    public void setZipcode ( Integer zipcode ) {
        this.zipcode = zipcode;
    }

    public String getCountry () {
        return country;
    }

    public void setCountry ( String country ) {
        this.country = country;
    }
}
