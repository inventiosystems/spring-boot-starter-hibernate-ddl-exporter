# Hibernate DDL Exporter

This project provides a Spring Boot Starter for easy generation of Schema Export using Hibernate.
The aim is to simplify the process of generating a schema to point of this being a drop-in extension of any Spring and Hibernate based project.

In the spirit of Spring Boot, this starter aims to provide sensible defaults when generating a schema export.
If you wish to customize the process please refer to the section[Customisation](#Customisation)below

## Library Matrix
This starter is developed and tested with the following versions of the main libraries

|Spring Boot|Hibernate|Hibernate Validator|JPA|Reference|
|---|---|---|---|---|
|2.1.10.RELEASE|5.2.17.Final|6.0.18.Final|2.0.0|[Spring Data JPA POM](https://github.com/spring-projects/spring-data-jpa/blob/2.1.x/pom.xml), [Spring Boot Starter Validation at Maven Central](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation/2.1.10.RELEASE)|

## How-To-Use

In order to enable the use of the Spring Boot Starter add the following dependency to your project.
#####Maven:
```xml
<dependency>
    <groupId>com.inventiosystems</groupId>
    <artifactId>spring-boot-starter-hibernate-ddl-exporter</artifactId>
    <version>0.0.1</version>
</dependency>
```
#####Gradle:
```groovy
compile group: 'com.inventiosystems', name: 'spring-boot-starter-hibernate-ddl-exporter', version: '0.0.1'
```

Once the dependency has been added it is advised that you create a test which initializes your Spring Boot Application (including the JPA Configuration). 
Ensure that the following property is correctly configured to enable the generation of the schema export.
```properties
spring.jpa.hibernate.generate-ddl = true
```
The schema export will be generate in the subdirectory ``db``. This can be customized using the property
```properties
hibernate.schema.location = <PATH_TO_DESIRED_LOCATION>
```
__NB__: The export will still be placed in the subdirectory ``db`` of the specified location.

For a working example please refer to the Spring Boot Starter Hibernate DDL Exporter Demo. 

## Inner Workings
The Spring Boot Starter Hibernate DDL Exporter is based on the standard Hibernate DDL Exporter.
Essentially the starter scans the class path of the project for all available database drivers
and then creates an instance of a HibernateSchemaExporter accordingly. Each of the create 
HibernateSchemaExporters are provided to the DdlGenerator for executing. 

## Customisation


## License
The Spring Boot Starter Hibernate DDL Exporter is available under the Apache 2.0 license. 
For full information please refer to[LICENSE.md](LICENSE.md)