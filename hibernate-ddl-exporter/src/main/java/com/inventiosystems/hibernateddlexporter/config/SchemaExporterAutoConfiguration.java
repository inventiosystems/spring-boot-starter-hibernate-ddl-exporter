/*-
 * #%L
 * Spring Boot Starter Hibernate DDL Exporter
 * %%
 * Copyright (C) 2017 Inventio Systems
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.inventiosystems.hibernateddlexporter.config;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.inventiosystems.hibernateddlexporter.config.properties.HibernateDdlExporterProperties;
import com.inventiosystems.hibernateddlexporter.exporter.HibernateSchemaExporter;
import com.inventiosystems.hibernateddlexporter.generator.DdlGenerator;
import com.inventiosystems.hibernateddlexporter.generator.DefaultDdlGenerator;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.cfg.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;

/**
 * Auto configuration for schema exporters.
 */
@Configuration
@PropertySource ( value = "classpath:hibernate-ddl-exporter.properties" )
@ConditionalOnProperty ( name = "spring.jpa.hibernate.generate-ddl", havingValue = "true" )
@EnableConfigurationProperties ( value = HibernateDdlExporterProperties.class )
public class SchemaExporterAutoConfiguration {

    private final Logger logger = LoggerFactory.getLogger ( this.getClass () );

    @Value ( "${hibernate.schema.location:}" )
    private String customSchemaLocation;

    @Value ( "${spring.jpa.hibernate.generate-ddl.include-timestamp:true}" )
    private boolean includeTimestamp;

    @Autowired
    private DdlGenerator ddlGenerator;

    @Bean
    public DdlGenerator ddlGenerator ( AbstractEntityManagerFactoryBean entityManagerFactoryBean, HibernateDdlExporterProperties properties ) {
        List<HibernateSchemaExporter> schemaExports = new ArrayList<> ();

        for ( Map.Entry<String, HibernateDdlExporterProperties.ExporterProperties> entry : properties.getExporters ().entrySet () ) {
            final BootstrapServiceRegistry bsr = new BootstrapServiceRegistryBuilder ().build ();
            final StandardServiceRegistryBuilder ssrBuilder = new StandardServiceRegistryBuilder ( bsr );
            final Map<String, Object> settings = new HashMap<> ( entityManagerFactoryBean.getObject ().getProperties () );
            ssrBuilder.applySettings ( settings );
            ssrBuilder.applySetting ( Environment.DIALECT, entry.getValue ().getDialect () );
            StandardServiceRegistry serviceRegistry = ssrBuilder.build ();

            final MetadataSources metadataSources = new MetadataSources ( serviceRegistry );
            final List<String> managedClassNames = entityManagerFactoryBean.getPersistenceUnitInfo ().getManagedClassNames ();
            Collections.sort ( managedClassNames );
            for ( String clazz : managedClassNames ) {
                logger.info ( "Adding entity {}", clazz );
                try {
                    metadataSources.addAnnotatedClass ( Class.forName ( clazz ) );
                } catch ( ClassNotFoundException e ) {
                    e.printStackTrace ();
                }
            }
            logger.info ( "Added {} entities", metadataSources.getAnnotatedClasses ().size () );

            final MetadataImplementor metadata = (MetadataImplementor) metadataSources.buildMetadata ();

            final HibernateSchemaExporter schemaExport = new HibernateSchemaExporter ( serviceRegistry, metadata, entry.getKey () );
            if ( entry.getValue ().getDelimiter () == null ) {
                schemaExport.setDelimiter ( properties.getDefaultDelimiter () );
            } else {
                schemaExport.setDelimiter ( entry.getValue ().getDelimiter () );
            }

            schemaExports.add ( schemaExport );
        }

        return new DefaultDdlGenerator ( schemaExports, customSchemaLocation, includeTimestamp );
    }


    @PostConstruct
    public void init () {
        if ( ddlGenerator != null ) {
            ddlGenerator.executeExport ();
        }
    }
}
