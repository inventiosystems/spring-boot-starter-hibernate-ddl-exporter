/*-
 * #%L
 * Spring Boot Starter Hibernate DDL Exporter
 * %%
 * Copyright (C) 2017 Inventio Systems
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.inventiosystems.hibernateddlexporter.exporter;

import java.util.EnumSet;

import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

public class HibernateSchemaExporter extends SchemaExport {

    private final String name;

    private final StandardServiceRegistry serviceRegistry;

    private final MetadataImplementor metadataImplementor;

    public HibernateSchemaExporter ( StandardServiceRegistry serviceRegistry, MetadataImplementor metadata, String name ) {
        this.name = name;
        this.metadataImplementor = metadata;
        this.serviceRegistry = serviceRegistry;
    }

    public String getName () {
        return this.name;
    }

    public void execute ( EnumSet<TargetType> targetTypes, Action action ) {
        super.execute ( targetTypes, action, this.metadataImplementor, this.serviceRegistry );
    }
}
